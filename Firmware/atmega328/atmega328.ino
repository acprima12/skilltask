#include <Button.h>

#define pin_POT     A0  //10K/ 100K Variable Resistor
#define pin_START   A1 // Run
#define pin_STOP    A2 // Stop
#define pin_PWM     11 // PWM

#define PULLUP      true
#define INVERT      true
#define DEBOUNCE_MS 5

bool flag_RUN, flag_STOP, flag_START;

int read_ADC = 0;
int duty_cycle;
int duty_cycle_percent;
int previous_duty_cycle = 0;
int interval_data = 10;
char data_BUFF[5];
String data_STRING;

unsigned long previousMillis = 0;

Button PB_START(pin_START, PULLUP, INVERT, 5);
Button PB_STOP(pin_STOP, PULLUP, INVERT, 5);

void ss_function(){
  PB_START.read();
  PB_STOP.read();

  if(PB_START.wasPressed()){
    flag_START = 1;
  }
  else{
    flag_START = 0;
  }

  if(PB_STOP.wasPressed()){
    flag_STOP = 1;
  }
  else{
    flag_STOP = 0;
  }

  if(flag_START == 1 && flag_STOP == 0){
    flag_RUN = 1;
  }
  else if(flag_START == 0 && flag_STOP == 1){
    flag_RUN = 0;
  }
}

void read_adc(){
  read_ADC = analogRead(pin_POT);
  
  duty_cycle = map(read_ADC, 0, 1023, 0, 255);
  duty_cycle_percent = map(read_ADC, 0, 1023, 0, 100);
}

void send_serial(){
  //kelipatan 10/ modulo 10
  if((duty_cycle_percent % 10) == 0){
    int current_duty_cycle = duty_cycle_percent;
    
    if(abs(current_duty_cycle - previous_duty_cycle) >= interval_data){
      previous_duty_cycle = current_duty_cycle;

      data_STRING = String (duty_cycle_percent);
      data_BUFF[3];
      data_STRING.toCharArray(data_BUFF, 4);
      Serial.write(data_BUFF, 3);
    }
  }
}

void output_motor(){
  if(flag_RUN == 1){
    analogWrite(pin_PWM, duty_cycle);
  }
  else{
    analogWrite(pin_PWM, 0);
  }
}

void setup(){
  Serial.begin(9600);
  
  pinMode(pin_POT, INPUT);
  
  pinMode(pin_START, INPUT_PULLUP);
  pinMode(pin_STOP, INPUT_PULLUP);

  pinMode(pin_PWM, OUTPUT);
  
  delay(1000);  
}

void loop(){
  ss_function();
  read_adc();
  send_serial();
  output_motor();
}
