#include <Arduino.h>
#include "SPI.h"
#include "SD.h"
#include "RTClib.h"
#include <ESP8266WiFi.h>
#include <time.h>
#include <PubSubClient.h>
#include <algorithm>

#define chipSelect  D8

const char* willTopic = "/MOTOR/DC/WILLTOPIC";
const char* topic_1   = "/MOTOR/DC/DUTY_CYCLE";
const char* clientId  = "DC_MOTOR_25";

const char* ssid        = "L.A. Boys";        //nama wifi
const char* pass        = "koetaksunping5";   //password wifi
const char* broker      = "192.168.5.46";     //broker
unsigned long mqtt_time = 2000;               //sending time

boolean boot = true;
char data_RECEIVE[4];
String data_SEND;
unsigned long previousMillis = 0;
unsigned long interval = 250;
bool flag_wifi, flag_mqtt;
unsigned long lastMsg = 0;
long currentTime, lastTime;

WiFiClient espClient;
PubSubClient client(espClient);
RTC_DS1307 rtc;

//wifi even
WiFiEventHandler wifiConnectHandler;
WiFiEventHandler wifiDisconnectHandler;

void initWiFi() {
  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, pass);
  Serial.print("Connecting to WiFi ..");
  while (WiFi.status() != WL_CONNECTED) {
    Serial.print('.');
    delay(1000);
  }
  Serial.println(WiFi.localIP());
}

void onWifiConnect(const WiFiEventStationModeGotIP& event) {
  Serial.println("Connected to Wi-Fi sucessfully.");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

void onWifiDisconnect(const WiFiEventStationModeDisconnected& event) {
  Serial.println("Disconnected from Wi-Fi, trying to connect...");
  WiFi.disconnect();
  WiFi.begin(ssid, pass);
}

//setup wifi
boolean setup_wifi(){
  delay(10);
  
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);
  
  // delete old config
  WiFi.disconnect(true);

  delay(1000);

  //Register event handlers
  wifiConnectHandler = WiFi.onStationModeGotIP(onWifiConnect);
  wifiDisconnectHandler = WiFi.onStationModeDisconnected(onWifiDisconnect);
   
  initWiFi();
  Serial.print("RRSI: ");
  Serial.println(WiFi.RSSI());

  Serial.println();
  Serial.println();
  Serial.println("Wait for WiFi... ");

  Serial.println();
  Serial.print("WIFI                : ");
  Serial.println(ssid);
  Serial.print("password            : ");
  Serial.println(pass);
  Serial.print("Local IP (esp32)    : ");
  Serial.println(WiFi.localIP());
  Serial.print("Subnet Mask         : ");
  Serial.println(WiFi.subnetMask());
  Serial.print("Gateway IP (router) : ");
  Serial.println(WiFi.gatewayIP());
  Serial.print("MAC address         : ");
  Serial.println(WiFi.macAddress());
  Serial.print("Server              : ");
  Serial.println(broker);
  Serial.println();
  
  delay(10);
  Serial.println(millis());
  Serial.println();
}

//mqtt
void callback(char* topic, byte* payload, unsigned int length){
  Serial.print("Received messages : ");
  Serial.println(topic);
  
  for(int i=0; i<length; i++){
    Serial.print((char) payload[i]);
  }
  Serial.println();
}

//setup mqtt
void setup_mqtt(){
  client.setServer(broker, 1883);
  client.setCallback(callback);
}

//setup rtc
void setup_rtc(){
  if (!rtc.begin()) {
    Serial.println("Couldn't find RTC");
    while (1);
  }

  if (!rtc.isrunning()) {
    Serial.println("RTC lost power, lets set the time!");
    rtc.adjust(DateTime(F(__DATE__), F(__TIME__)));
  }
}

//setup sd card
void setup_sd(){
  if (!SD.begin(chipSelect)) {
    Serial.println("Initialization failed!");
    while (1);
  }
}

//loop data
void loop_data(){
  Serial.readBytes(data_RECEIVE, 3);
  
  data_SEND = String (data_RECEIVE);
}

//loop save file
void loop_save(){
  String dataString = "";

  DateTime now = rtc.now();

  dataString += String(now.unixtime());
  dataString += ",";
  dataString += "Duty_Cycle";
  dataString += ",";
  dataString += data_SEND;
  
  File dataFile = SD.open("datalog.txt", FILE_WRITE);

  if (dataFile) {
    dataFile.println(dataString);
    dataFile.close();
    Serial.println(dataString);
  }
  else {
    Serial.println("error opening datalog.txt");
  }
}

//mqtt
void reconnect(){
  if(!client.connected()){
    Serial.print("\nConnecting to ");
    Serial.println(broker);
    
    for(int i = 0; i <= 55; i++){
      Serial.print(".");
    }
    for(int i = 0; i <= 55; i++){
      Serial.print(".");
    }

    if(client.connect(clientId, willTopic, 1, true, "disconected")){
      Serial.print("\nConnected to ");
      Serial.println(broker);
    }
    else{
      Serial.println("\nTrying to connect again");
      delay(5000);
    }
  }
}

//loop mqtt
void loop_mqtt(){
  if (boot == true){
    if (setup_wifi()){
      client.setServer(broker, 1883);
      client.setCallback(callback);
    }
    boot = false;
  }
  
  if (!client.connected()){
    reconnect();
    flag_mqtt = 0;
  }
  else{
    flag_mqtt = 1;
  }
  client.loop();

  currentTime = millis();
  if(currentTime - lastTime > mqtt_time){
    client.publish(topic_1, data_SEND.c_str());
    client.publish(willTopic, "connected", true);

    Serial.println();
    Serial.println(millis());
    Serial.println("|| DATA SEND ||");
    Serial.println(data_SEND);
    Serial.println("|| --------- ||");
    Serial.println();
    
    lastTime = millis();
  }
}
void setup() {
  Serial.begin(9600);

  setup_wifi();
  setup_mqtt();
  setup_rtc();
  setup_sd();

  delay(1000);
}

void loop() {
  loop_data();
  loop_save();
  loop_mqtt();
}
